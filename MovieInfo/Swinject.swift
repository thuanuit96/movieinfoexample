

import UIKit
import SwinjectStoryboard
import Swinject

extension SwinjectStoryboard
{
    class func setup ()
    {
        
        defaultContainer.register(MovieService.self) { resolveable in
            return MovieStore.shared
        }
        defaultContainer.register(MovieListViewViewModel.self) { resolveable in
            return MovieListViewViewModel(movieService: resolveable.resolve(MovieService.self)!)
        }
        defaultContainer.storyboardInitCompleted(MovieListViewController.self) { (resolveable, c) in
            c.movieListViewViewModel = resolveable.resolve(MovieListViewViewModel.self)

        }
       
    }
}
