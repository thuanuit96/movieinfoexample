//
//  MovieListViewController.swift
//  MovieInfo
//
//  Created by Alfian Losari on 10/03/19.
//  Copyright © 2019 Alfian Losari. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class MovieListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var movieListViewViewModel: MovieListViewViewModel!
    let disposeBag = DisposeBag()
 
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        
            movieListViewViewModel.movies.drive(onNext: {[unowned self] (_) in
                self.tableView.reloadData()
            }).disposed(by: disposeBag)
            
            movieListViewViewModel.isFetching.drive(activityIndicatorView.rx.isAnimating)
                .disposed(by: disposeBag)
            
            movieListViewViewModel.error.drive(onNext: {[unowned self] (error) in
                self.infoLabel.isHidden = !self.movieListViewViewModel.hasError
                self.infoLabel.text = error
            }).disposed(by: disposeBag)
            
        movieListViewViewModel.setEndpoint(endpoint: segmentedControl.rx.selectedSegmentIndex
                                            .map { Endpoint(index: $0) ?? .nowPlaying }
                                            .asDriver(onErrorJustReturn: .nowPlaying))
            setupTableView()
    }

    private func setupTableView() {
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.register(UINib(nibName: "MovieCell", bundle: nil), forCellReuseIdentifier: "MovieCell")
    }
    
}

extension MovieListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieListViewViewModel.numberOfMovies
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as! MovieCell
        if let viewModel = movieListViewViewModel.viewModelForMovie(at: indexPath.row) {
            cell.configure(viewModel: viewModel)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let viewModel = movieListViewViewModel.viewModelForMovie(at: indexPath.row) {
            let detail = MovieDetailViewController()
            detail.movieViewViewModel = viewModel
            self.navigationController?.pushViewController(detail, animated: true)
        }
    }
    
}
