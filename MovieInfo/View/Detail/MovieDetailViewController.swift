//
//  MovieDetailViewController.swift
//  MovieInfo
//
//  Created by asto_vn2 on 2/7/21.
//  Copyright © 2021 Alfian Losari. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbTilte: UILabel!
    @IBOutlet weak var des: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    var movieViewViewModel : MovieViewViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        guard let viewModel = movieViewViewModel  else  {
            return
        }
        
        setData(viewModel : viewModel)
    }
    
    func setData(viewModel : MovieViewViewModel ) {
        lbTilte.text = viewModel.title
        des.text = viewModel.overview
        releaseDate.text = viewModel.releaseDate
        img.kf.setImage(with: viewModel.posterURL)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
